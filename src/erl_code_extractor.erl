-module(erl_code_extractor).

%% API exports
-export([extract/1, extract_to_json/1, extract_to_json/2]).

%%====================================================================
%% API functions
%%====================================================================

extract_to_json(Path, Output) ->
  Res = extract_to_json(Path),
  file:write_file(Output, Res).

extract_to_json(Path) ->
  Res = extract(Path),
  jsx:encode(#{funcalls => Res}).

extract(Path) ->
  case filelib:is_dir(Path) of
    true ->
      filelib:fold_files(Path, "\.(erl|beam)$", true, fun extract/2, []);
    false ->
      extract(Path, [])
  end.

%%====================================================================
%% Internal functions
%%====================================================================
extract(File, State) ->
  Ast = case filename:extension(File) of
          ".erl" ->
            {ok, A} = epp:parse_file(File, []),
            A;
          ".beam" ->
            {ok, {_, [{abstract_code, {raw_abstract_v1, A}}]}} = beam_lib:chunks(File, [abstract_code]),
            A
        end,

  Functions = mapfilter(fun parse_function/1, Ast),
  [Module|_] = [Name || {attribute, _, module, Name} <- Ast],
  [#{file => list_to_binary(File), functions => Functions, module => Module} | State].

parse_function({function, Line, Name, Arity, Body}) ->
  Parsed = mapfilter(fun parse_clause/1, Body),
  Flat = lists:flatten(Parsed),
  case Flat of
    [] -> skip;
    _ ->
      #{
        line => Line,
        name => Name,
        arity => Arity,
        calls => Flat
      }
  end;

parse_function(_) ->
  skip.

parse_clause({clause, _Line, _Pattern, _Guard, Body}) ->
  mapfilter(fun parse_item/1, Body);

parse_clause(_) ->
  skip.

parse_item(Item) ->
  parse_hacky(Item).

parse_hacky({call, Line, {remote, Line, Mod, Fun}, Args}) ->
  Fn = make_fun(Line, Mod, Fun, length(Args)),
  case {Fn, parse_hacky(Args)} of
    {skip, Other} -> Other;
    {_, skip} -> Fn;
    {_, List} when is_list(List) -> [Fn | List];
    {_, Other} -> [Fn, Other]
  end;

%% parse_hacky({call, Line, Fun, _Args}) ->
%%   {local, Line, Fun};

parse_hacky({'fun', Line, {Mod, Fun, Arity}}) ->
  make_fun(Line, Mod, Fun, Arity);

parse_hacky(Item) when is_tuple(Item) ->
  parse_hacky(tuple_to_list(Item));

parse_hacky(Item) when is_list(Item) ->
  mapfilter(fun parse_hacky/1, Item);

parse_hacky(_) ->
  skip.

mapfilter(Fun, List) ->
  mapfilter(Fun, List, []).

mapfilter(_Fun, [], Acc) ->
  lists:reverse(Acc);

mapfilter(Fun, [H|T], Acc) ->
  case Fun(H) of
    skip -> mapfilter(Fun, T, Acc);
    Other -> mapfilter(Fun, T, [Other | Acc])
  end.

make_fun(Line, {atom, _, Mod}, {atom, _, Fun}, Arity) ->
  #{
    line => Line,
    module => Mod,
    function => Fun,
    arity => Arity
  };

make_fun(_, _, _, _) ->
  skip.
